const run = require('./run');

run().then((result) => {
  process.exit(result);
}).catch((err) => {
  console.error(err);
  process.exit(1);
});
