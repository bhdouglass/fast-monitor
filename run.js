const fast = require('fast-cli/api');
const dns = require('dns');
const format = require('date-fns/format');
const isNumber = require('is-number');
const fs = require('fs');

function writeResults(data, err) {
  fs.appendFileSync('./results/results.csv', [
    format(new Date(), 'yyyy-MM-dd HH:mm:ss'),
    err ? 'Failure' : 'Success',
    data ? data.downloadSpeed : 0,
    data ? data.downloadUnit : '',
    data ? data.uploadSpeed : 0,
    data ? data.uploadUnit : '',
    err ? err : '',
  ].map((item) => {
    if (!item || isNumber(item)) {
        return item;
    }

    return `"${item}"`;
  }).join(',') + '\n');
}

async function run() {
  dns.lookup('fast.com', err => {
    if (err && err.code === 'ENOTFOUND') {
      writeResults(null, 'Unable to perform dns lookup');
      return 2;
    }
  });

  try {
    let data = null;
    await fast({ measureUpload: true }).forEach((results) => {
      data = results;
    });

    writeResults(data);
    return 0;
  }
  catch (err) {
    writeResults(null, err.message);
    return 1;
  }
}

module.exports = run;
