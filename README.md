# Fast Monitor

Some scripts and a docker image to monitor your internet speed with fast.com

## Run

- `npm run start`: This will get results from fast.com and put them in results/results.csv
- `npm run cron`: This will get results from fast.com and put them in results/results.csv every hour

## Run with Docker

```docker run -v `pwd`/results:/srv/fast-monitor/results -it bhdouglass/fast-monitor```

## License

Copyright (C) 2020 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
