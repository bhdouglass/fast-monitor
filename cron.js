const CronJob = require('cron').CronJob;
const run = require('./run');

const job = new CronJob('0 0 * * * *', run);
job.start();
