FROM alekzonder/puppeteer:latest
LABEL maintainer="Brian Douglass"

ADD . /srv/fast-monitor/
VOLUME [ "/srv/fast-monitor/results" ]

WORKDIR /srv/fast-monitor/
CMD [ "node", "./cron.js" ]
